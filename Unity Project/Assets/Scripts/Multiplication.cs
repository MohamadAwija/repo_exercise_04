﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Multiplication : MonoBehaviour
    {
        public int numberOne = 2;
        public float numberTwo = 2.5f;
        public float sum;


        public void FirstSum()
        {
            sum = numberOne * numberTwo;
            Debug.Log(sum);
        }

        public void SecondSum(int numberThree, float numberFour)
        {
            sum = numberThree * numberFour;
            Debug.Log(sum);
        }

        public float ThirdSum(int numberFive, float numberSix)
        {
            float sumTwo = numberFive * numberSix;
            return sumTwo;
        }

        void Start()
        {
            FirstSum();
            SecondSum(3, 4);
            sum = ThirdSum(5, 4);
            Debug.Log(sum);
        }
    }
}
