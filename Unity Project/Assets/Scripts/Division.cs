﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Division : MonoBehaviour
    {
        public int numberOne = 1;
        public float numberTwo = 8.0f;
        public float sum;


        public void FirstSum()
        {
            sum = numberOne / numberTwo;
            Debug.Log(sum);
        }

        public void SecondSum(int numberThree, float numberFour)
        {
            sum = numberThree / numberFour;
            Debug.Log(sum);
        }

        public float ThirdSum(int numberFive, float numberSix)
        {
            float sumTwo = numberFive / numberSix;
            return sumTwo;
        }

        void Start()
        {
            FirstSum();
            SecondSum(1, 2);
            sum = ThirdSum(10, 3);
            Debug.Log(sum);
        }
    }
}
