﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Subtraction : MonoBehaviour
    {
        public int numberOne = 1;
        public float numberTwo = 2.0f;
        public float sum;


        public void FirstSum()
        {
            sum = numberOne - numberTwo;
            Debug.Log(sum);
        }

        public void SecondSum(int numberThree, float numberFour)
        {
            sum = numberThree - numberFour;
            Debug.Log(sum);
        }

        public float ThirdSum(int numberFive, float numberSix)
        {
            float sumTwo = numberFive - numberSix;
            return sumTwo;
        }

        void Start()
        {
            FirstSum();
            SecondSum(5, 10);
            sum = ThirdSum(20, 30);
            Debug.Log(sum);
        }
    }
}